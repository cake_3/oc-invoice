fetch('./data/sp-students.json')
  .then(data => {
    return data.json()
  })
  .then(selfPaidStudents => {
    const sessions = extractData(studentSessions)
    const invoice = invoiceTotal(sessions, levels, selfPaidStudents)
    renderInvoice(invoice)
  })
  .catch((e) => {
    console.error(e)
  })
