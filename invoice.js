const table = document.getElementById('table')
const levels = {
  'level-1': 30,
  'level-2': 35,
  'level-3': 40
}

const studentSessions = {}

function extractData(sessions) {
  let i = 0
  while(true) {      
    const date = table.children[1].children[i].cells[0].innerText.split(" ")
    const entryDate = date[0] +'-'+ date[2]
    const status = table.children[1].children[i].cells[2].innerText.trim()

    if (status !== 'Canceled') {
      const studentName = table.children[1].children[i].cells[1].innerText.trim()
      const entry = {
        student: studentName,
        selfPaid: selfPaidStudents.includes(studentName),
        status: table.children[1].children[i].cells[2].innerText.trim(),
        type: table.children[1].children[i].cells[3].innerText.trim(),
        level: 'level-'+ table.children[1].children[i].cells[4].innerText.trim()
      }

      sessions[entryDate] = sessions[entryDate] || []
      sessions[entryDate].push(entry)
    }

    ++i
    if (i == table.children[1].children.length) {
      return sessions
    }
  }
}

function invoiceTotal (sessions, levels, selfPaidStudents) { 
  const invoice = {}
  
  Object.entries(sessions).forEach(([month, sessions]) => {
    const fixedTotal = 30 * selfPaidStudents.length
    invoice[month] = invoice[month] || {
      fixedTotal: fixedTotal,
      sessionTotal: 0,
      total: fixedTotal,
      sessions: {} 
    }
   
    sessions.forEach(item => {
      let levelType = item.selfPaid ? `self-paid-${item.level}` : item.level
      let levelPrice = item.selfPaid ? levels[item.level] / 2 : levels[item.level]
      let levelLabel = item.selfPaid ? `Self paid ${item.level}` : item.level
      
      if (item.status === 'Absent student') {
        levelType = item.selfPaid ? `no-show-${item.level}-self-paid` : `no-show-${item.level}`
        levelLabel = item.selfPaid ? `No show ${item.level} - self paid` : `No show ${item.level}`
        levelPrice = levelPrice / 2
      }
      
      if (invoice[month].sessions[levelType]) {
        invoice[month].sessions[levelType].label = levelLabel
        invoice[month].sessions[levelType].number = ++invoice[month].sessions[levelType].number,
        invoice[month].sessions[levelType].unit = levelPrice,
        invoice[month].sessions[levelType].total = invoice[month].sessions[levelType].total + levelPrice
      } else {
        invoice[month].sessions[levelType] =  {
          label: levelLabel,
          number: 1,
          unit: levelPrice,
          total: levelPrice
        }
      }
      
      invoice[month].sessionTotal += levelPrice
      invoice[month].total += levelPrice
    })
  })
  
  return invoice
}

function renderInvoice(invoice) {
  const invoiceElement = document.getElementById('invoice')
  
  Object.entries(invoice).forEach(([month, item]) => {
    const invoiceMonth = document.createElement('h2')
    const invoiceBreak = document.createElement('hr')
    const invoiceSessionTotal = document.createElement('p')
    const invoiceFixedTotal = document.createElement('p')
    const invoiceTotal = document.createElement('p')
    const invoiceTable = document.createElement('table')
    const invoiceTableHeader = document.createElement('thead')
    const invoiceTableTr = document.createElement('tr')
    const invoiceTableTd1 = document.createElement('td')
    const invoiceTableTd2 = document.createElement('td')
    const invoiceTableTd3 = document.createElement('td')
    const invoiceTableBody = document.createElement('tbody')
    
    invoiceMonth.innerText = month
    invoiceTableTd1.innerText = 'Session type'
    invoiceTableTd2.innerText = 'Number of sessions'
    invoiceTableTd3.innerText = 'Unit price'
    invoiceTable.appendChild(invoiceTableHeader)
    invoiceTable.appendChild(invoiceTableBody)
    invoiceTableHeader.appendChild(invoiceTableTr)
    invoiceTableTr.appendChild(invoiceTableTd1)
    invoiceTableTr.appendChild(invoiceTableTd2)
    invoiceTableTr.appendChild(invoiceTableTd3)

    Object.values(item.sessions).forEach(session => {
      const sessionTableTr = document.createElement('tr')
      const sessionlabelElement = document.createElement('td')
      const sessionNumberElement = document.createElement('td')
      const sessionUnit = document.createElement('td')
      
      sessionlabelElement.innerText = session.label
      sessionNumberElement.innerText = session.number
      sessionUnit.innerText = session.unit
      sessionTableTr.appendChild(sessionlabelElement)
      sessionTableTr.appendChild(sessionNumberElement)
      sessionTableTr.appendChild(sessionUnit)
      invoiceTableBody.appendChild(sessionTableTr)
    })
    
    invoiceSessionTotal.innerHTML = `<strong>Session total: € ${item.sessionTotal}</strong>`
    invoiceFixedTotal.innerHTML = `<strong>Fixed total: € ${item.fixedTotal}</strong>`
    invoiceTotal.innerHTML = `<strong>Total: € ${item.total}</strong>`
    invoiceElement.appendChild(invoiceMonth)
    invoiceElement.appendChild(invoiceTable)
    invoiceElement.appendChild(invoiceBreak)
    invoiceElement.appendChild(invoiceSessionTotal)
    invoiceElement.appendChild(invoiceFixedTotal)
    invoiceElement.appendChild(invoiceTotal)
  })
}

